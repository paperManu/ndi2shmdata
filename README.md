# NDI2shmdata

Tool that convert [shmdata](https://gitlab.com/sat-metalab/shmdata) to [NewTek's NDI](http://ndi.newtek.com), and _vice-versa_.

NDI2Shmdata currently uses version 4.6 of the NDI SDK.

Example
=======

List visible NDI streams:

```bash
ndi2shmdata -L
```

Get a stream "ARES (chestbro)" that will be sent to two shmdata: `/tmp/rvid` and `/tmp/raudio`:

```bash
ndi2shmdata -n "ARES (chestbro)" -v /tmp/rvid -a /tmp/raudio -d
```

View the stream with gst-launch:

```bash
gst-launch -v shmdatasrc socket-path=/tmp/rvid ! videoconvert ! xvimagesink sync=false
```

Publish a stream 'chestbro' from shmdatas

```bash
shmdata2ndi  -d -v /tmp/vidshmdata -a /tmp/audioshmdata -n chestbro
```

For more detailed information, get help from the commands using the `-h` flag:

```bash
ndi2shmdata -h
shmdata2ndi -h
```

Build from sources 
==================

This has been tested with Ubuntu 20.04. Git and shmdata must be installed on your system.
You need to accept NDI license for the code to compile. The License Agreement text is available [here](ndi-sdk/NDI\ License\ Agreement.txt) and can be accepted with the cmake variable "ACCEPT_NDI_LICENSE" set to "ON".

The NDI library depends on the Avahi library. The shmdata library is also needed:

```bash
sudo apt-add-repository ppa:sat-metalab/metalab
sudo apt install libshmdata-dev libavahi-client-dev
```

Then to compile the `ndi2shmdata` and `shmdata2ndi` tools:

```bash
git clone https://gitlab.com/sat-metalab/ndi2shmdata.git
cd ndi2shmdata && mkdir build && cd build
cmake -DACCEPT_NDI_LICENSE=ON ..
make
sudo make install
```
