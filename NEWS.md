# NDI2shmdata Changelog

ndi2shmdata [0.6.0] - 2021-08-11
--------------------------------

* 🐛 Fix command line arguments crash
* ✨ Add flags to select output quality from ndi2shmdata

ndi2shmdata [0.5.0] - 2021-04-08
--------------------------------

Bug Fixes:
* 🐛 Fixed ndi2shmdata not updating shmdata caps when data format changes

Documentation:
* 📝 Fixed documentation to add mandatory dependencies

NDI:
* 👽 Update NDI SDK to 4.6.2


ndi2shmdata [0.4.0] - 2020-06-10
--------------------------------

Documentation:
* 📝 Update release how to

NDI:
* 👽 Update NDI SDK to 4.5.2


ndi2shmdata [0.3.0] - 2020-02-07
--------------------------------

New Feature:
* ✨ Add user-configurable timeout for stream discovery

Bug Fixes:
* 🐛 Refactor signal handling to avoid undefined behaviors

NDI:
* 👽  Updating NDI SDK to 4.1


ndi2shmdata [0.2.0] - 2019-04-26
--------------------------------

New features:
* Multichannel audio support (S16LE and F32LE)
* port to NDI 3.8

Bug Fixes:
* fix installed command printing usage regardless of arguments


ndi2shmdata [0.1.0] - 2019-02-11
--------------------------------

New features:
* command line listing of available NDI streams
* ndi2shmdata subscribes to a NDI stream using its name, and write video frames to a shmdata
* shmdata2ndi subscribes to a shmdata and publishes it as a NDI stream
