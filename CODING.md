CONTRIBUTING
======

Coding style
------------

We use Google C++ Style Guide, as decribed [here](https://google.github.io/styleguide/cppguide.html), with two excpetions:
* a function call’s arguments will either be all on the same line or will have one line each. 
* a function declaration’s or function definition’s parameters will either all be on the same line or will have one line each.

Note that you may find configuration file for your editor [here](https://github.com/google/styleguide).

Contributing
------------

Please send your merge request at the [sat-metalab' gitlab account](https://gitlab.com/sat-metalab/ndi2shmdata). If you do not know how to make a merge request, gitlab provides some [help about creating a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

Branching strategy with git
---------------------------

The [master](https://gitlab.com/sat-metalab/ndi2shmdata/tree/master) branch contains releases. Validated new developments are into the [develop](https://gitlab.com/sat-metalab/ndi2shmdata/tree/develop) branch.

Modifications are made into a dedicated branch that need to be merged into the develop branch through a gitlab merge request. When you modification is ready, you need to prepare your merge request as follow:
* Update your develop branch. 
```
git fetch
git checkout develop
git pull origin develop
```
* Go back to your branch and rebase onto develop. Note that it would be appreciated that you only merge request from a single commit (i.e interactive rebase with squash and/or fixup before pushing).
```
git rebase -i develop
```
