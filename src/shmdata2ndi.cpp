// MIT License

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <signal.h>
#include <unistd.h>

#include <iostream>
#include <memory>
#include <vector>

#include <Processing.NDI.Lib.h>
#include <shmdata/console-logger.hpp>
#include <shmdata/follower.hpp>
#include <shmdata/type.hpp>

using namespace shmdata;

static NDIlib_send_instance_t NDI_sender;
static auto logger = std::make_unique<ConsoleLogger>();
static std::unique_ptr<Follower> vfollower;
static std::unique_ptr<Follower> afollower;
static auto stream_name = std::string();
static auto video_shmpath = std::string();
static auto audio_shmpath = std::string();
static std::vector<float> aframes{}; 
static auto audio_caps = std::string();
static auto audio_is_short = false;
static auto debug = false;
static NDIlib_video_frame_v2_t NDI_video_frame;
static NDIlib_audio_frame_v2_t NDI_audio_frame;

static volatile sig_atomic_t stopped = 0;
extern "C" void stop_process(int signal_number) { stopped = signal_number; }

void clean_up() {
  // shmdata cleaning
  vfollower.reset();
  afollower.reset();
  logger.reset();
  // NDI cleaning
  NDIlib_send_destroy(NDI_sender);
  NDIlib_destroy();
}

void usage(const char *prog_name){
  printf("%s publishes shmdata as a Newtek NDI stream (http://ndi.newtek.com)\n"
         "usage: %s [-d] -v videoshmpath -a audioshmpath\n"
         "  -v <shmpath> : path to a video shmdata\n"
         "  -a <shmpath> : path to an audio shmpath\n"
         "  -n <name>    : set NDI stream name\n"
         "  -d           : enable debug\n"
         , prog_name
         , prog_name);
  exit(0);
}

void make_video_follower() {

  NDI_video_frame.FourCC = NDIlib_FourCC_type_BGRX;

  // creating shmdata follower (video)
  vfollower = std::make_unique<Follower>(
      video_shmpath,
      [&](void *data, size_t /*size*/) {  // on data
        NDI_video_frame.p_data = static_cast<uint8_t *>(data);
        NDIlib_send_send_video_v2(NDI_sender, &NDI_video_frame);
      },
      [&](const std::string& shmtype){  // on connect
        auto type = shmdata::Type(shmtype);
        // check whether this is a video shmdata
        if (type.name() != "video/x-raw") {
          logger->error("incompatible shmdata type: %", type.name());
          logger->error("expected shmdata type is 'video/x-raw'");
          stop_process(1);
        }
        // get shmdata properties
        auto props = type.get_properties();
        // width
        auto width_it = props.find("width");
        if (props.end() == width_it) {
          logger->error("incompatible shmdata: width property not found");
          stop_process(1);
        }
        NDI_video_frame.xres = std::any_cast<int>(width_it->second);
        // height
        auto height_it = props.find("height");
        if (props.end() == height_it) {
          logger->error("incompatible shmdata: height property not found");
          stop_process(1);
        }
        NDI_video_frame.yres = std::any_cast<int>(height_it->second);
        // pixel format
        auto format_it = props.find("format");
        if (props.end() == format_it) {
          logger->error("incompatible shmdata: format property not found");
          stop_process(1);
        }
        auto format = std::any_cast<std::string>(format_it->second);
        if (format == "I420") {
          NDI_video_frame.FourCC = NDIlib_FourCC_type_I420;
        } else if(format == "UYVY") {
          NDI_video_frame.FourCC = NDIlib_FourCC_type_UYVY;
        } else if(format == "YV12") {
          NDI_video_frame.FourCC = NDIlib_FourCC_type_YV12;
        } else if(format == "NV12") {
          NDI_video_frame.FourCC = NDIlib_FourCC_type_NV12;
        } else if(format == "BGRA") {
          NDI_video_frame.FourCC = NDIlib_FourCC_type_BGRA;
        } else if(format == "RGBA") {
          NDI_video_frame.FourCC = NDIlib_FourCC_type_RGBA;
        } else {
          logger->error("incompatible shmdata: format not supported: %", format);
          logger->error("(format supported: I420, UYVY, YV12, NV12, BGRA, RGBA)");
        }
        // success ! we have enough information for processing this shmdata
      },
      [&](){
      },
      logger.get());
}

void make_audio_follower() {
  afollower = std::make_unique<Follower>(
      audio_shmpath,
      [&](void *rawdata, size_t size) {  // on data
        aframes.resize(size);
        // rearrangement of interleaved 'rawdata' into one-channel-after-an-other in 'data'
        // S16LE
        if (audio_is_short) { 
          NDI_audio_frame.no_samples = size / (sizeof(short) * NDI_audio_frame.no_channels);
          NDI_audio_frame.channel_stride_in_bytes = 2 * size / NDI_audio_frame.no_channels;
          short *data = static_cast<short*>(rawdata);
          float sample = 0;
          for (auto channel = 0; channel < NDI_audio_frame.no_channels; ++ channel) {
            for (auto i = 0; i < NDI_audio_frame.no_samples; ++i) {
              sample = data[NDI_audio_frame.no_channels * i + channel] / 32768.f;
              if( sample > 1.f ) sample = 1.f;
              else if ( sample < -1.f ) sample = -1.f;
              aframes[channel * NDI_audio_frame.no_samples + i] = sample;
            }
          }
        } else {  
          // F32LE
          NDI_audio_frame.no_samples = size / (sizeof(float) * NDI_audio_frame.no_channels);
          NDI_audio_frame.channel_stride_in_bytes = size / NDI_audio_frame.no_channels;
          float *data = static_cast<float*>(rawdata);
          for (auto channel = 0; channel < NDI_audio_frame.no_channels; ++ channel) {
            for (auto i = 0; i < NDI_audio_frame.no_samples; ++i) {
              aframes[channel * NDI_audio_frame.no_samples + i] = data[NDI_audio_frame.no_channels * i + channel];
            }
          }
        }
        // set data and send
        NDI_audio_frame.p_data = aframes.data();
        NDIlib_send_send_audio_v2(NDI_sender, &NDI_audio_frame);
      },
      [&](const std::string& shmtype){  // on connect
        if (!audio_caps.empty() && audio_caps != shmtype) {
          logger->error("does not support change of audio format.\n was %\n "
                        "changed to %\n",
                        audio_caps, shmtype);
          stop_process(1);
        }
        audio_caps = shmtype;
        auto type = shmdata::Type(shmtype);
        // check this is a video shmdata
        if (type.name() != "audio/x-raw") {
          logger->error("incompatible shmdata type for audio: %", type.name());
          logger->error("expected shmdata type is 'audio/x-raw'");
          stop_process(1);
        }
        // get shmdata properties
        auto props = type.get_properties();
        // rate
        auto rate_it = props.find("rate");
        if (props.end() == rate_it) {
          logger->error("incompatible shmdata: rate property not found");
          stop_process(1);
        }
	NDI_audio_frame.sample_rate = std::any_cast<int>(rate_it->second);
        // channels
        auto channels_it = props.find("channels");
        if (props.end() == channels_it) {
          logger->error("incompatible shmdata: channels property not found");
          stop_process(1);
        }
	NDI_audio_frame.no_channels = std::any_cast<int>(channels_it->second);
        // format
        auto format_it = props.find("format");
        if (props.end() == format_it) {
          logger->error("incompatible shmdata: format property not found");
          stop_process(1);
        }
        if (std::any_cast<std::string>(format_it->second) != "F32LE") {
          audio_is_short = true;
          if (std::any_cast<std::string>(format_it->second) != "S16LE") {
            logger->error("incompatible shmdata: audio format must be F32LE or S16LE");
            stop_process(1);
          }
        }
      },
      [&](){
      },
      logger.get());
}

int main(int argc, char* argv[]) {
  signal(SIGINT, stop_process);
  signal(SIGABRT, stop_process);
  signal(SIGQUIT, stop_process);
  signal(SIGTERM, stop_process);
  
  char *shmpath = nullptr;
  opterr = 0;
  int c = 0;
  while ((c = getopt (argc, argv, "dn:v:a:")) != -1)
    switch (c)
      {
        case 'd':
          debug = true;
          break;
        case 'n':
          stream_name = std::string(optarg);
          break;
        case 'v':
          video_shmpath = std::string(optarg);
          break;
        case 'a':
          audio_shmpath = std::string(optarg);
          break;
        case '?':
          break;
        default:
          usage("shmdata2ndi");
      }

  if (video_shmpath.empty() && audio_shmpath.empty())
    usage("shmdata2ndi");

  logger = std::make_unique<ConsoleLogger>();
  logger->set_debug(debug);

  // NDI initialization
  if (!NDIlib_initialize()) {
    logger->error("could not initialize NDI");
    return 1;
  }

  NDIlib_send_create_t create_params_send;
  create_params_send.p_ndi_name = stream_name.empty() ? shmpath : stream_name.c_str();
  create_params_send.p_groups = nullptr;
  create_params_send.clock_video = false;
  create_params_send.clock_audio = false;
  NDI_sender = NDIlib_send_create(&create_params_send);
  if (!NDI_sender) {
    logger->error("could not initialize NDI sender");
    return 1;
  }

  if (!video_shmpath.empty()) {
    make_video_follower();
  }

  if (!audio_shmpath.empty()) {
    make_audio_follower();
  }

  while (true) {
    if (stopped > 0) {
      clean_up();
      exit(stopped);
    }
    sleep(1);
  }
  return 0;
}
