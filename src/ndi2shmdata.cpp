// MIT License

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
#include <signal.h>
#include <unistd.h>

#include <Processing.NDI.Lib.h>
#include <shmdata/console-logger.hpp>
#include <shmdata/type.hpp>
#include <shmdata/writer.hpp>

using namespace shmdata;

// video shmdata
static const std::string base_video_type = "video/x-raw";
static std::unique_ptr<Writer> vshmwriter = nullptr;
static Type video_type = Type(base_video_type);
static size_t pixel_size = 0;
static std::string video_shmpath = std::string();

// audio shmdata
static const std::string base_audio_type = "audio/x-raw";
static std::unique_ptr<Writer> ashmwriter = nullptr;
static Type audio_type = Type(base_audio_type);
static std::string audio_shmpath = std::string();
static std::vector<short> audio_frames_short;
static std::vector<float> audio_frames_float;
static bool convert_to_S16LE = false;

// NDI
static std::string ndi_name = std::string();
static NDIlib_recv_instance_t pNDI_recv;

// command line behaviour
static std::unique_ptr<ConsoleLogger> logger = std::make_unique<ConsoleLogger>();
static bool debug = false;
static bool fastest_video_format = false;
static bool hq_video_format = false;
static bool list = false;
static bool wait_and_list = false;
static int timeout_ms = 1000;

static volatile sig_atomic_t stopped = 0;
extern "C" void stop_process(int signal_number) { stopped = signal_number; }

void clean_up() {
  // NDI cleaning
  NDIlib_destroy();
  // shmdata cleaning
  vshmwriter.reset();
  ashmwriter.reset();
  logger.reset();
}

void usage(const char *prog_name) {
  printf("%s receive a Newtek NDI stream (http://ndi.newtek.com) and write it to a shmdata\n"
         "usage: %s [-d] [-v <video_shmpath>] [-a <audio_shmpath>] -n <ndi_name> \n"
         "  -n <name>          : subscribe to the NDI stream called 'name'\n"
         "  -v <video_shmpath> : set the shmdata path for the video stream\n"
         "  -a <audio_shmpath> : set the shmdata path for the audio stream\n"
         "  -s                 : convert to short 16 bit (S16LE). Default is float (F32LE)\n"
         "  -f                 : decode video with the fastest available color format (UYVY, alpha channel is dropped)\n"
         "  -b                 : decode video with the highest quality available color format (UYVY, alpha channel is dropped)\n"
         "  -d                 : enable debug\n"
         "\n"
         "You can list existing NDI streams with the following options:\n"
         "   -l<timeout_ms>    : wait and listen to current NDI stream(s) and exit. Default timeout is %s ms\n"
         "   -L                : wait and log arrival and departure of NDI stream(s)\n",
         prog_name, prog_name, std::to_string(timeout_ms).c_str());
  exit(0);
}

/**
 * Extract a FourCC string from the NDI FourCC code, and update the shmdata::Type
 * given as parameter
 * \param type Type to update with the FourCC code
 * \param video_frame NDI video frame to get the code from
 */
void update_video_format(Type& type, const NDIlib_video_frame_v2_t &video_frame) {
  switch (video_frame.FourCC) {
  // YCbCr color space
  case NDIlib_FourCC_type_UYVY:
    type.set_prop("format", std::string("UYVY"));
    pixel_size = 2;
    break;
  case NDIlib_FourCC_type_UYVA:
    // From NDI documentation:
    // YCbCr + Alpha color space, using 4:2:2:4.
    // In memory there are two separate planes. The first is a regular
    // UYVY 4:2:2 buffer. Immediately following this in memory is a
    // alpha channel buffer.
    type.set_prop("format", std::string("UYVY"));
    // By using a pixel size of 2, we effectively drop the alpha channel
    // to go back to a more standard UYVY format
    pixel_size = 2;
    break;
  // 4:2:0 formats
  case NDIlib_FourCC_type_YV12:
    type.set_prop("format", std::string("YV12"));
    pixel_size = 1.5;
    break;
  case NDIlib_FourCC_type_NV12:
    type.set_prop("format", std::string("NV12"));
    pixel_size = 1.5;
    break;
  case NDIlib_FourCC_type_I420:
    type.set_prop("format", std::string("I420"));
    pixel_size = 1.5;
    break;
  // BGRA
  case NDIlib_FourCC_type_BGRA:
    type.set_prop("format", std::string("BGRA"));
    pixel_size = 4;
    break;
  case NDIlib_FourCC_type_BGRX:
    type.set_prop("format", std::string("BGRx"));
    pixel_size = 4;
    break;
  // RGBA
  case NDIlib_FourCC_type_RGBA:
    type.set_prop("format", std::string("RGBA"));
    pixel_size = 4;
    break;
  case NDIlib_FourCC_type_RGBX:
    type.set_prop("format", std::string("RGBx"));
    pixel_size = 4;
    break;
  default:
    logger->error("unknown video format");
  }
}

/**
 * Extract a shmdata::Type from a NDI video frame
 * \param video_frame NDI video frame
 * \return Returrn a shmdata::type
 */
Type extract_video_type(const NDIlib_video_frame_v2_t &video_frame) {
  Type new_video_type = Type(base_video_type);

  update_video_format(new_video_type, video_frame);
  new_video_type.set_prop("width", video_frame.xres);
  new_video_type.set_prop("height", video_frame.yres);
  new_video_type.set_prop("framerate", "fraction",
                      std::to_string(video_frame.frame_rate_N) + "/" +
                          std::to_string(video_frame.frame_rate_D));
  new_video_type.set_prop("pixel-aspect-ratio", "fraction", "1/1");

  if (video_frame.frame_format_type == NDIlib_frame_format_type_progressive)
    new_video_type.set_prop("interlace-mode", std::string("progressive"));
  else
    new_video_type.set_prop("interlace-mode", std::string("interleaved"));

  if(video_frame.p_metadata && !std::string(video_frame.p_metadata).empty())
    new_video_type.set_prop("metadata", video_frame.p_metadata);

  return new_video_type;
}

/**
 * Write the video frame to a shmdata
 * \param video_frame NDI video frame
 */
void write_video_frame(NDIlib_video_frame_v2_t &video_frame) {
  if (video_shmpath.empty()) {
    static bool empty_shmpath_notified = false;
    if (!empty_shmpath_notified) {
      empty_shmpath_notified = true;
      logger->warning("NDI stream with video frames but no shmdata path has been given for video (-v option)");
    }
    return;
  }

  // Generate a new shmdata writer if needed
  auto new_video_type = extract_video_type(video_frame);
  if (!vshmwriter || video_type.str() != new_video_type.str()) {
    video_type = new_video_type;
    // creating shmdata follower
    logger->debug("video shmdata type %", video_type.str());
    vshmwriter.reset();
    vshmwriter = std::make_unique<Writer>(video_shmpath,
                                          video_frame.xres * video_frame.yres * pixel_size,
                                          video_type.str(),
                                          logger.get());
  }
  vshmwriter->copy_to_shm(video_frame.p_data, video_frame.xres * video_frame.yres * pixel_size);
 }

/**
 * Extract a shmdata::Type from a NDI audio frame
 * \param audio_frame NDI audio frame
 * \return Returrn a shmdata::type
 */
Type extract_audio_type(const NDIlib_audio_frame_v2_t &audio_frame) {
  Type new_audio_type = Type(base_audio_type);

  new_audio_type.set_prop("rate", audio_frame.sample_rate);
  new_audio_type.set_prop("channels", audio_frame.no_channels);
  new_audio_type.set_prop("layout", "interleaved");
  if (convert_to_S16LE)
    new_audio_type.set_prop("format", "S16LE");
  else
    new_audio_type.set_prop("format", "F32LE");
  new_audio_type.set_prop("channel-mask", "bitmask", "0x0000000000000000");

  return new_audio_type;
}

/**
 * Write the audio frame to a shmdata
 * \param audio_frame NDI audio frame
 */
void write_audio_frame(const NDIlib_audio_frame_v2_t &audio_frame) {
  if (audio_shmpath.empty()) {
    static bool empty_shmpath_notified = false;
    if (!empty_shmpath_notified) {
      empty_shmpath_notified = true;
      logger->warning("NDI stream with audio frames but no shmdata path has been given for audio (-a option)");
    }
    return;
  }

  // Generate a new shmdata writer if needed
  auto new_audio_type = extract_audio_type(audio_frame);
  if (!ashmwriter || audio_type.str() != new_audio_type.str()) {
    audio_type = new_audio_type;
    // creating the shmdata follower
    ashmwriter.reset();
    ashmwriter = std::make_unique<Writer>(audio_shmpath,
                                          audio_frame.channel_stride_in_bytes * audio_frame.no_channels,
                                          audio_type.str(),
                                          logger.get());
  }

  if (!convert_to_S16LE) {
    // Allocate enough space for interleaved buffer
    NDIlib_audio_frame_interleaved_32f_t audio_frame_32f_interleaved;
    audio_frames_float.resize(audio_frame.no_samples * audio_frame.no_channels);
    audio_frame_32f_interleaved.p_data = audio_frames_float.data();
    // Convert it
    NDIlib_util_audio_to_interleaved_32f_v2(&audio_frame, &audio_frame_32f_interleaved);
    // Copy to the shmdata
    ashmwriter->copy_to_shm(audio_frame_32f_interleaved.p_data, sizeof(float) * audio_frame.no_samples * audio_frame.no_channels);
    return;
  }
    
  // Allocate enough space for 16bpp interleaved buffer
  NDIlib_audio_frame_interleaved_16s_t audio_frame_16bpp_interleaved;
  audio_frame_16bpp_interleaved.reference_level = 4;	// We are going to have 4dB of headroom
  audio_frames_short.resize(audio_frame.no_samples * audio_frame.no_channels);
  audio_frame_16bpp_interleaved.p_data = audio_frames_short.data();
  
  // Convert it
  NDIlib_util_audio_to_interleaved_16s_v2(&audio_frame, &audio_frame_16bpp_interleaved);

  // Copy to the shmdata
  ashmwriter->copy_to_shm(audio_frame_16bpp_interleaved.p_data, sizeof(short) * audio_frame.no_samples * audio_frame.no_channels);
}

int main(int argc, char *argv[]) {
  signal(SIGINT, stop_process);
  signal(SIGABRT, stop_process);
  signal(SIGQUIT, stop_process);
  signal(SIGTERM, stop_process);

  opterr = 0;
  int c = 0;
  while ((c = getopt(argc, argv, "bdfv:n:l::La:s")) != -1)
    switch (c) {
    case 'b':
      hq_video_format = true;
      break;
    case 'd':
      debug = true;
      break;
    case 'f':
      fastest_video_format = true;
      break;
    case 'v':
      video_shmpath = std::string(optarg);
      break;
    case 'n':
      ndi_name = std::string(optarg);
      break;
    case 'l':
      list = true;
      if (optarg != nullptr) {
        try
        {
          timeout_ms = std::stoi(optarg);
        }
        catch (...)
        {
          std::cout << "Bad value for timeout (-l) parameter\n";
        }
      }
      break;
    case 'L':
      wait_and_list = true;
      break;
    case 'a':
      audio_shmpath = std::string(optarg);
      break;
    case 's':
      convert_to_S16LE = true;
      break;
    case '?':
      break;
    default:
      usage("ndi2shmdata");
    }

  logger->set_debug(debug);
    
  // NDI initialization
  if (!NDIlib_initialize()) {
    logger->error("could not initialize NDI");
    return 1;
  }

  // Create a finder
  NDIlib_find_instance_t pNDI_find = NDIlib_find_create();
  if (!pNDI_find) {
    logger->error("could not initialize NDI find\n");
    return 1;
  }

  // variable used for source discovery
  uint32_t no_sources = 0;
  const NDIlib_source_t* p_sources = NULL;

  // list source
  if (list) {
    sleep(timeout_ms / 1000.);
    p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
    for (uint32_t i = 0; i < no_sources; i++)
      std::cout << p_sources[i].p_ndi_name << '\n';
    return 0;
  }

  // wait and list new soures
  if (wait_and_list) {
    std::vector<std::string> current_sources;
    while(true) {
      std::vector<std::string> tmp;
      NDIlib_find_wait_for_sources(pNDI_find, 1000/* One second */);
      p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
      for (uint32_t i = 0; i < no_sources; i++) {
        tmp.push_back(p_sources[i].p_ndi_name);
        auto found = std::find(current_sources.begin(), current_sources.end(), p_sources[i].p_ndi_name);
        if (found != current_sources.end()) current_sources.erase(found);
        else std::cout << "source arrival : " <<  p_sources[i].p_ndi_name << '\n';
      }
      for (auto &it: current_sources) std::cout << "source departure : " << it << '\n';
      std::swap(current_sources,tmp);
    }
    return 0;
  }

  // check if all mandatory options are specified
  if (ndi_name.empty()) {
    logger->set_debug(true);
    logger->error("missing NDI stream name");
    usage("ndi2shmdata");
    return 1;
  }
  
  // wait for specified source
  bool source_found = false;
  uint32_t source_index = 0;
  while (!source_found) {
    NDIlib_find_wait_for_sources(pNDI_find, 1000/* One second */);
    p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
    for (uint32_t i = 0; i < no_sources; i++) {
      if (ndi_name == p_sources[i].p_ndi_name) {
        source_index = i;
        source_found = true;
      }
    }
  }

  // We now have our source, so we create a receiver to look at it.
  NDIlib_recv_create_v3_t recv_create_obj = NDIlib_recv_create_v3_t(
    /* source_to_connect_toc = */ nullptr,
    /* color_formatc = */ NDIlib_recv_color_format_UYVY_RGBA,
    /* bandwidthc = */ NDIlib_recv_bandwidth_highest,
    /* allow_video_fieldsc = */ true,
    /* p_ndi_recv_namec = */ nullptr
  );

  if (fastest_video_format)
    recv_create_obj.color_format = NDIlib_recv_color_format_fastest;
  if (hq_video_format)
    recv_create_obj.color_format = NDIlib_recv_color_format_best;

  pNDI_recv = NDIlib_recv_create_v3(&recv_create_obj);
  if (!pNDI_recv) {
    logger->error("could not initialize NDI recv");
    return 1;
  }
  // Connect to our sources
  NDIlib_recv_connect(pNDI_recv, &p_sources[source_index]);

  // Destroy the NDI finder. We needed to have access to the pointers to p_sources[0]
  NDIlib_find_destroy(pNDI_find);


  while (true) {
    if (stopped > 0) {
      clean_up();
      exit(stopped);
    }
    NDIlib_video_frame_v2_t video_frame;
    NDIlib_audio_frame_v2_t audio_frame;
    switch (NDIlib_recv_capture_v2(pNDI_recv, &video_frame, &audio_frame,
                                   nullptr, 5000)) {
      case NDIlib_frame_type_none: // No data
        logger->warning("No data received");
        break;
      case NDIlib_frame_type_video: // Video data
        //logger->debug("Video data received (%x%)", std::to_string(video_frame.xres),
        //           std::to_string(video_frame.yres));
        write_video_frame(video_frame);
        NDIlib_recv_free_video_v2(pNDI_recv, &video_frame);
        break;
      case NDIlib_frame_type_audio: // Audio data
        //logger->debug("Audio data received (% samples)", std::to_string(audio_frame.no_samples));
        write_audio_frame(audio_frame);
        NDIlib_recv_free_audio_v2(pNDI_recv, &audio_frame);
        break;
      case NDIlib_frame_type_metadata: // Metadata
        break;
      case NDIlib_frame_type_error: // Errors
        break;
      case NDIlib_frame_type_status_change: // Status change
        break;
      case NDIlib_frame_type_max: // Max
        break; 
    }
  }
  return 0;
}
